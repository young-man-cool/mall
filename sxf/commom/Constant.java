package com.sxf.mall.common;

import com.google.common.collect.Sets;
import com.sxf.mall.exception.MallException;
import com.sxf.mall.exception.MallExceptionEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 *     常量值
 */
@Component
public class Constant {
    public static final String MALL_USER = "mall_user";

    public static String FILE_UPLOAD_DIR;

    @Value("${file.upload.dir}")
    public void setFileUploadDir(String fileUploadDir) {
        FILE_UPLOAD_DIR = fileUploadDir;
    }

    public interface ProductListOrderBy{
        Set<String> PRICE_ACS_DESC = Sets.newHashSet("price desc","price asc");
    }

    public interface SaleStatus{
        int  NOT_SALE = 0; //商品下架状态
        int  SALE = 1;//商品上架状态
    }

    public interface CartChecked{
        int UN_CHECKED = 0;//购物车未被选中状态
        int CHECKED = 1; //购物车被选中状态
    }

    public enum OrderStatusEnum{
        CANCELED(0,"用户已取消"),
        NOT_PAID(1,"未付款"),
        PAID(2,"已付款"),
        DELIVERED(3,"已发货"),
        FINISHED(4,"交易完成");

        private int code;
        private String value;

        OrderStatusEnum(int code, String value) {
            this.value = value;
            this.code = code;
        }
        public static OrderStatusEnum codeOf(int code) {
            for (OrderStatusEnum orderStatusEnum : values()) {
                if (orderStatusEnum.getCode() == code) {
                    return orderStatusEnum;
                }
            }
            throw new MallException(MallExceptionEnum.NO_ENUM);
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
