package com.sxf.mall.util;

import org.apache.tomcat.util.codec.binary.Base64;
import sun.security.provider.MD5;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5哈希加密工具
 */
public class MD5Util {
    public static String getMD5Str(String strValue) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        String SALT = "10086asd...";
        return Base64.encodeBase64String(md5.digest((strValue+SALT).getBytes()));
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        String md5Str = MD5Util.getMD5Str("10086789000");
        System.out.println(md5Str);
    }
}
