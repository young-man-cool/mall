package com.sxf.mall.controller;

        import com.github.pagehelper.PageInfo;
        import com.sxf.mall.common.ApiRestResponse;
        import com.sxf.mall.common.Constant;
        import com.sxf.mall.exception.MallExceptionEnum;
        import com.sxf.mall.model.dao.CategoryMapper;
        import com.sxf.mall.model.pojo.Category;
        import com.sxf.mall.model.pojo.User;
        import com.sxf.mall.model.request.AddCategoryReq;
        import com.sxf.mall.model.request.UpdateCategoryReq;
        import com.sxf.mall.model.vo.CategoryVO;
        import com.sxf.mall.service.CategoryService;
        import com.sxf.mall.service.UserService;
        import io.swagger.annotations.ApiOperation;
        import org.springframework.beans.BeanUtils;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.web.bind.annotation.RequestBody;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestParam;
        import org.springframework.web.bind.annotation.ResponseBody;

        import javax.servlet.http.HttpSession;
        import javax.validation.Valid;
        import javax.validation.constraints.Max;
        import java.util.List;

/**
 *  目录controller
 */
@Controller
public class CategoryController {
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    UserService userService;
    @Autowired
    CategoryService categoryService;

    /**
     * 后台添加目录
     */
    @ApiOperation("后台添加目录")
    @RequestMapping("/admin/category/add")
    @ResponseBody
    public ApiRestResponse addCategory(HttpSession session, @Valid @RequestBody AddCategoryReq addCategoryReq) {
        User currentUser = (User)session.getAttribute(Constant.MALL_USER);
        if (currentUser == null) {
            return ApiRestResponse.error(MallExceptionEnum.NEED_LOGIN);
        }
        // 校验是否为管理员
        boolean adminRole = userService.checkAdminRole(currentUser);
        if (adminRole){
            // 管理员执行操作
            categoryService.add(addCategoryReq);
            return ApiRestResponse.success();
        }else {
            return ApiRestResponse.error(MallExceptionEnum.NEED_ADMIN);
        }
    }

    /**
     *   后台修改目录
     */
    @ApiOperation("后台修改目录")
    @RequestMapping("/admin/category/update")
    @ResponseBody
    public ApiRestResponse updateCategory(HttpSession session, @Valid @RequestBody UpdateCategoryReq updateCategoryReq) {
        User currentUser = (User)session.getAttribute(Constant.MALL_USER);
        if (currentUser == null) {
            return ApiRestResponse.error(MallExceptionEnum.NEED_LOGIN);
        }
        // 校验是否为管理员
        boolean adminRole = userService.checkAdminRole(currentUser);
        if (adminRole){
            // 管理员执行操作
            Category category = new Category();
            BeanUtils.copyProperties(updateCategoryReq,category);
            categoryService.update(category);
            return ApiRestResponse.success();
        }else {
            return ApiRestResponse.error(MallExceptionEnum.NEED_ADMIN);
        }
    }

    /**
     *   删除分类
     */
    @ApiOperation("后台删除目录")
    @RequestMapping("/admin/category/delete")
    @ResponseBody
    public ApiRestResponse deletecategory(@RequestParam Integer id){
            categoryService.delete(id);
            return ApiRestResponse.success();
    }

    /**
     *  后台目录列表
     */
    @ApiOperation("后台目录列表")
    @RequestMapping("/admin/category/list")
    @ResponseBody
    public ApiRestResponse listCategoryForAdmin(@RequestParam Integer pageNum, @RequestParam Integer pageSize){
        PageInfo pageInfo = categoryService.listForAdmin(pageNum, pageSize);
        return ApiRestResponse.success(pageInfo);
    }

    /**
     *  前台目录列表
     */
    @ApiOperation("前台目录列表")
    @RequestMapping("/category/list")
    @ResponseBody
    public ApiRestResponse listCategoryForCustomer(){
        List<CategoryVO> categoryVOS = categoryService.listForCustomer(0);
        return ApiRestResponse.success(categoryVOS);
    }
}
