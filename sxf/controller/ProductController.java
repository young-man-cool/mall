package com.sxf.mall.controller;

import com.github.pagehelper.PageInfo;
import com.sxf.mall.common.ApiRestResponse;
import com.sxf.mall.model.pojo.Product;
import com.sxf.mall.model.request.ProductListReq;
import com.sxf.mall.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *   前台商品Controller
 */
@RestController
public class ProductController {
    @Autowired
    ProductService productService;

    /**
     *   商品详情
     */
    @ApiOperation("商品详情")
    @GetMapping("product/detail")
    public ApiRestResponse detail(@RequestParam Integer id){
        Product detail = productService.detail(id);
        return ApiRestResponse.success(detail);
    }
    /**
     *   商品列表
     */
    @ApiOperation("商品列表")
    @GetMapping("product/list")
    public ApiRestResponse list(ProductListReq productListReq){
        PageInfo list = productService.list(productListReq);
        return ApiRestResponse.success(list);
    }

}
