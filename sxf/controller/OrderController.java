package com.sxf.mall.controller;

import com.github.pagehelper.PageInfo;
import com.sxf.mall.common.ApiRestResponse;
import com.sxf.mall.model.request.AddOrderReq;
import com.sxf.mall.model.vo.OrderVO;
import com.sxf.mall.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 *  订单controller
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    OrderService orderService;

    /**
     * 创建订单
     */
    @PostMapping("/create")
    @ApiOperation("创建订单")
    public ApiRestResponse create(@RequestBody @Valid AddOrderReq addOrderReq) {
        String orderNo = orderService.create(addOrderReq);
        return ApiRestResponse.success(orderNo);
    }
    /**
     *  前台：订单详情
     */
    @GetMapping("/detail")
    @ApiOperation("前台订单详情")
    public ApiRestResponse detail(@RequestParam String orderNo) {
        OrderVO orderVO = orderService.detail(orderNo);
        return ApiRestResponse.success(orderVO);
    }
    /**
     *  前台：订单列表
     */
    @GetMapping("/list")
    @ApiOperation("前台订单列表")
    public ApiRestResponse list(@RequestParam Integer pageNum,@RequestParam Integer pageSize) {
        PageInfo pageInfo = orderService.listForCustomer(pageNum, pageSize);
        return ApiRestResponse.success(pageInfo);
    }
    /**
     *  前台：订单取消
     */
    @PostMapping("/cancel")
    @ApiOperation("前台订单取消")
    public ApiRestResponse cancel(@RequestParam String orderNo) {
        orderService.cancel(orderNo);
        return ApiRestResponse.success();
    }
    /**
     *  生成支付二维码
     */
    @PostMapping("/qrcode")
    @ApiOperation("生成支付二维码")
    public ApiRestResponse qrcode(@RequestParam String orderNo) {
        String pngAddress = orderService.qrcode(orderNo);
        return ApiRestResponse.success(pngAddress);
    }
    /**
     *  支付接口
     */
    @GetMapping("/pay")
    @ApiOperation("支付接口")
    public ApiRestResponse pay(@RequestParam String orderNo) {
        orderService.pay(orderNo);
        return ApiRestResponse.success();
    }
}
