package com.sxf.mall.controller;

import com.github.pagehelper.PageInfo;
import com.sxf.mall.common.ApiRestResponse;
import com.sxf.mall.filter.UserFilter;
import com.sxf.mall.model.vo.CartVO;
import com.sxf.mall.service.CartService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *   购物车controller
 */
@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    CartService cartService;

    /**
     *  购物车列表
     */
    @GetMapping("/list")
    @ApiOperation("购物车列表")
    public ApiRestResponse<List<CartVO>> list(){
        Integer userId = UserFilter.currentUser.getId();
        List<CartVO> cartVOList = cartService.list(userId);
        return ApiRestResponse.success(cartVOList);
    }
    /**
     * 添加商品到购物车
     */
    @PostMapping("/add")
    @ApiOperation("添加商品到购物车")
    public ApiRestResponse add(@RequestParam Integer productId, @RequestParam Integer count){
        Integer userId = UserFilter.currentUser.getId();
        List<CartVO> cartList = cartService.add(userId, productId, count);
        return ApiRestResponse.success(cartList);
    }
    /**
     *  更新商品到购物车
     */
    @PostMapping("/update")
    @ApiOperation("更新商品到购物车")
    public ApiRestResponse update(@RequestParam Integer productId, @RequestParam Integer count){
        Integer userId = UserFilter.currentUser.getId();
        List<CartVO> cartList = cartService.update(userId, productId, count);
        return ApiRestResponse.success(cartList);
    }
    /**
     *  删除购物车商品
     */
    @PostMapping("/delete")
    @ApiOperation("删除购物车商品")
    public ApiRestResponse delete(@RequestParam Integer productId){
        Integer userId = UserFilter.currentUser.getId();
        List<CartVO> cartList = cartService.delete(userId,productId);
        return ApiRestResponse.success(cartList);
    }
    /**
     *   选中/不选中购物车的某个商品
     */
    @PostMapping("/select")
    @ApiOperation(" 选中/不选中购物车的某个商品")
    public ApiRestResponse select(@RequestParam Integer productId ,@RequestParam Integer selected ){
        Integer userId = UserFilter.currentUser.getId();
        List<CartVO> cartList = cartService.select(userId,productId,selected);
        return ApiRestResponse.success(cartList);
    }
    /**
     *   全选/全不选购物车的某个商品
     */
    @PostMapping("/selectAll")
    @ApiOperation(" 全选/全不选购物车的某个商品")
    public ApiRestResponse selectAll(@RequestParam Integer selected ){
        Integer userId = UserFilter.currentUser.getId();
        List<CartVO> cartList = cartService.selectAll(userId,selected);
        return ApiRestResponse.success(cartList);
    }
}
