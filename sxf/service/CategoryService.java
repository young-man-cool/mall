package com.sxf.mall.service;

import com.github.pagehelper.PageInfo;
import com.sxf.mall.model.pojo.Category;
import com.sxf.mall.model.request.AddCategoryReq;
import com.sxf.mall.model.request.UpdateCategoryReq;
import com.sxf.mall.model.vo.CategoryVO;

import java.util.List;

public interface CategoryService {
    void add(AddCategoryReq addCategoryReq);


    void  update(Category updateCategoryReq);

    void  delete(Integer id);

    PageInfo listForAdmin(Integer pageNum, Integer pageSize);

    List<CategoryVO> listForCustomer(Integer parentId);
}
