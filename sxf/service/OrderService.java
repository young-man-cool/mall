package com.sxf.mall.service;


import com.github.pagehelper.PageInfo;
import com.sxf.mall.model.request.AddOrderReq;
import com.sxf.mall.model.vo.OrderVO;

/**
 *   订单Service
 */
public interface OrderService {


    String create(AddOrderReq addOrderReq);

    OrderVO detail(String orderNo);

    PageInfo listForCustomer(Integer pageNum, Integer pageSize);

    void cancel(String orderNo);

    String qrcode(String orderNo);

    PageInfo listForAdmin(Integer pageNum, Integer pageSize);

    void pay(String orderNo);

    void delivered(String orderNo);

    // 完结订单
    void finish(String orderNo);
}
