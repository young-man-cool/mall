package com.sxf.mall.service;

import com.sxf.mall.exception.MallException;
import com.sxf.mall.model.pojo.User;

import java.security.NoSuchAlgorithmException;


public interface UserService {


    User getUser();
    void register(String userName,String password) throws MallException;

    User login(String userName, String password) throws  MallException;

    void  updateInformation(User user) throws MallException;

    boolean checkAdminRole(User user);
}
