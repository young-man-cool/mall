package com.sxf.mall.service.impl;

import com.github.pagehelper.PageInfo;
import com.sxf.mall.common.ApiRestResponse;
import com.sxf.mall.common.Constant;
import com.sxf.mall.exception.MallException;
import com.sxf.mall.exception.MallExceptionEnum;
import com.sxf.mall.model.dao.CartMapper;
import com.sxf.mall.model.dao.ProductMapper;
import com.sxf.mall.model.pojo.Cart;
import com.sxf.mall.model.pojo.Product;
import com.sxf.mall.model.vo.CartVO;
import com.sxf.mall.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 购物车Service实现类
 */
@Service
public class CartServiceImpl implements CartService {
    @Autowired
    ProductMapper productMapper;
    @Autowired
    CartMapper cartMapper;

    @Override
    public List<CartVO> list(Integer userId) {
        List<CartVO> cartVOS = cartMapper.selectByUserId(userId);
        for (int i = 0; i < cartVOS.size(); i++) {
            CartVO cartVO = cartVOS.get(i);
            cartVO.setTotalPrice(cartVO.getPrice() * cartVO.getQuantity());
        }
        return cartVOS;
    }

    @Override
    public List<CartVO> add(Integer userId, Integer productId, Integer count) {
        validProduct(productId, count);
        Cart cart = cartMapper.selectByUserIdAndProductId(userId, productId);
        if (cart == null) {
            //购物车中没有记录，需要添加
            cart = new Cart();
            cart.setProductId(productId);
            cart.setUserId(userId);
            cart.setQuantity(count);
            cart.setSelected(Constant.CartChecked.CHECKED);
            cartMapper.insertSelective(cart);
        } else {
            int newQuantity = cart.getQuantity() + count;
            cart.setQuantity(newQuantity);
            cart.setSelected(Constant.CartChecked.CHECKED);
            cartMapper.updateByPrimaryKeySelective(cart);
        }
        return this.list(userId);
    }

    private void validProduct(Integer productId, Integer count) {
        Product product = productMapper.selectByPrimaryKey(productId);
        if (product == null) {
            ApiRestResponse.error(MallExceptionEnum.PRODUCT_NOT_FIND);
        }
        if (product.getStatus() == Constant.SaleStatus.NOT_SALE) {
            ApiRestResponse.error(MallExceptionEnum.OFF_STATUS);
        }
        if (count > product.getStock()) {
            ApiRestResponse.error(MallExceptionEnum.NOT_STOCK);
        }
        ApiRestResponse.success();


    }

    @Override
    public List<CartVO> update(Integer userId, Integer productId, Integer count) {
        validProduct(productId, count);
        Cart cart = cartMapper.selectByUserIdAndProductId(userId, productId);
        if (cart == null) {
            //购物车中没有记录，无法修改
            throw new MallException(MallExceptionEnum.UPDATE_FAILED);
        } else {
            cart.setQuantity(count);
            cart.setSelected(Constant.CartChecked.CHECKED);
            cartMapper.updateByPrimaryKeySelective(cart);
        }
        return this.list(userId);
    }
    @Override
    public List<CartVO> delete(Integer userId, Integer productId) {
        Cart cart = cartMapper.selectByUserIdAndProductId(userId, productId);
        if (cart == null) {
            //购物车中没有记录，无法删除
            throw new MallException(MallExceptionEnum.DELETE_FAILED);
        } else {
            cartMapper.deleteByPrimaryKey(cart.getId());
        }
        return this.list(userId);
    }


    @Override
    public List<CartVO> select(Integer userId,Integer productId, Integer selected) {
        Cart cart = cartMapper.selectByUserIdAndProductId(userId,productId);
        if (cart == null) {
            //购物车中没有记录，无法选中
            throw new MallException(MallExceptionEnum.UPDATE_FAILED);
        } else {
            cartMapper.selectOrNot(userId,productId,selected);
        }
        return this.list(userId);
    }
    @Override
    public List<CartVO> selectAll(Integer userId, Integer selected) {
        cartMapper.selectOrNot(userId,null,selected);
        return this.list(userId);
    }
}

