package com.sxf.mall.exception;

/**
 *  异常枚举
 */
public enum MallExceptionEnum {
   NEED_USER_NAME(10001,"用户名不能为空"),
   NEED_PASSWORD(10002,"密码不能为空"),
   PASSWORD_TOO_SHORT(10003,"密码不能小于八位"),
   NAME_EXISTED(10004,"不允许重名"),
   INSERT_FAILED(10005,"注册失败，请重试"),
   WRONG_PASSWORD(10006,"账号或密码错误！"),
   NEED_LOGIN(10007,"用户未登录"),
   UPDATE_FAILED(10008,"修改失败"),
   NEED_ADMIN(10009,"无管理员权限"),
   ID_NOT_NULL(10010,"id不能为空"),
   CREATE_FAILED(10011,"新增失败"),
   REQUEST_PARAM_ERROR(10012,"参数错误"),
   PRODUCT_NOT_FIND(10013,"商品不存在"),
   DELETE_FAILED(10014,"删除失败"),
   MKDIR_FAILED(10015,"创建文件夹失败"),
   UPLOAD_FAILED(10016,"图片上传失败"),
   OFF_STATUS(10017,"商品已下架"),
   NOT_STOCK(10018,"商品库存不足"),
   CHECKED_NULL(10019,"已勾选商品为空"),
    NO_ENUM(10020,"枚举类未找到"),
    NO_ORDER(10021,"订单不存在"),
    NOT_YOU_ORDER(10022,"该订单不属于你，请查询自己的订单"),
    CANCEL_FAILED(10023,"订单当前状态无法取消"),
    PAY_FAILED(10024,"订单当前状态无法付款"),
    DELIVERED_FAILED(10025,"订单当前状态无法发货"),
    FINISH_FAILED(10026,"订单当前状态无法完结"),
   SYSTEM_ERROR(20000,"系统异常");


    /**
     * 异常码
     */
    Integer code;
    /**
     * 异常信息
     */
    String msg;

    MallExceptionEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
